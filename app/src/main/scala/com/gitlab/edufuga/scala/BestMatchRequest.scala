package com.gitlab.edufuga.scala

import io.circe.Decoder.Result
import io.circe.generic.semiauto._
import io.circe.{Decoder, Encoder, HCursor, Json}

import java.time.LocalDate
import java.time.format.DateTimeFormatter

/** Represents the request for finding the best rated Amazon products.
 *
 * @param start Start date of the Amazon reviews to consider (inclusive).
 * @param end End date of the Amazon reviews to consider (inclusive).
 * @param limit Maximum number of results to consider.
 * @param min_number_reviews Minimum number of reviews to consider. Products with a lower rating are thus left out.
 */
case class BestMatchRequest(start: LocalDate, end: LocalDate, limit: Int, min_number_reviews: Int)

/** Companion object providing an implicit decoding and encoding to and from a textual representation
 * of [[BestMatchRequest]]s.
 */
object BestMatchRequest {
  implicit val bestMatchRequestDecoder: Decoder[BestMatchRequest] = deriveDecoder[BestMatchRequest]
  implicit val bestMatchRequestEncoder: Encoder[BestMatchRequest] = deriveEncoder[BestMatchRequest]

  implicit val dateFormat : Encoder[LocalDate] with Decoder[LocalDate] =
    new Encoder[LocalDate] with Decoder[LocalDate] {
      private val formatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd.MM.yyyy")

      override def apply(d: LocalDate): Json = Encoder.encodeString.apply(d.format(formatter))

      override def apply(c: HCursor): Result[LocalDate] = Decoder.decodeString.map(
        s => LocalDate.parse(s, formatter)
      ).apply(c)
    }
}
