package com.gitlab.edufuga.scala

import java.time.LocalDateTime
import slick.jdbc.HsqldbProfile.api._

/** Represents the database table for [[ReviewEntity]] objects.
 *
 * @param tag [[https://scala-slick.org/doc/3.3.3/schemas.html Slick]]-specific way to specify ("mark") a row of the
 *            corresponding table.
 *            See [[https://stackoverflow.com/questions/27777862/what-is-tag-type-exactly/ this SO question]] for more
 *            information.
 */
class ReviewTable(tag: Tag) extends Table[ReviewEntity](tag, None, "REVIEWS") {
  val id = column[Option[Int]]("id", O.AutoInc, O.PrimaryKey)
  val asin = column[String]("asin")
  val helpfulPositive = column[Int]("helpfulPositive")
  val helpfulTotal = column[Int]("helpfulTotal")
  val overall = column[Double]("overall")
  val reviewText = column[String]("reviewText")
  val reviewerID = column[String]("reviewerID")
  val reviewerName = column[Option[String]]("reviewerName")
  val summary = column[String]("summary")
  val unixReviewTime = column[LocalDateTime]("unixReviewTime")
  override def * = (id, asin, helpfulPositive, helpfulTotal, overall, reviewText, reviewerID, reviewerName, summary, unixReviewTime) <> (ReviewEntity.tupled, ReviewEntity.unapply)
}
