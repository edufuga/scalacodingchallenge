package com.gitlab.edufuga.scala

import io.circe.{Decoder, Encoder}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

/** Represents the best match of an Amazon product.
 * @param asin Amazon product identifier.
 * @param averageRating Average rating of the Amazon product.
 */
case class BestMatch(asin: String, averageRating: Double)

/** Companion object providing an implicit decoding and encoding to and from a textual representation
 * of [[BestMatch]]es.
 */
object BestMatch {
  implicit val bestMatchResponseDecoder: Decoder[BestMatch] = deriveDecoder[BestMatch]
  implicit val bestMatchResponseEncoder: Encoder[BestMatch] = deriveEncoder[BestMatch]
}
