package com.gitlab.edufuga.scala

import com.twitter.finagle.Http
import com.twitter.util.Await

import java.nio.file.Paths

/**
 * Runs a webservice for returning the best rated products based on Amazon reviews.
 *
 * This webservice extracts the Amazon reviews from a given JSON file and imports them to a database.
 *
 * The absolute path to the JSON file must be given as the program's first command line argument.
 * Example: `/Users/eduard/Develop/ScalaCodingChallenge/app/src/main/resources/video_game_reviews_example.json`
 *
 * Once the webservice is running, you can send a HTTP POST request in order to obtain the best rated products matching
 * your query.
 * Example:
 * {{{
 * curl -X POST -H "Content-Type: application/json" \
 *   -d '{"start": "01.01.2010", "end": "31.12.2020", "limit": 2, "min_number_reviews": 2}' \
 *   http://localhost:8080/amazon/best-rated
 * }}}
 */
object BestMatchServiceApp extends App {
  if (args.length < 1) {
    throw new IllegalArgumentException("You must give me the path to the file with JSON reviews to use.")
  }

  println("Extracting reviews from the JSON file and importing them to the database.")
  val total = ReviewImporter().importReviewsFrom(Paths.get(args(0)))
  println("Imported " + total + " reviews.")

  println("Starting HTTP server with best match endpoint.")
  Await.ready(Http.server.serve(":8080", BestMatchService(BestMatchFinder().findBestMatches).service))
}
