package com.gitlab.edufuga.scala

import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, HCursor, Json}

/** Helpfulness rating of an Amazon review.
 *
 * @param positive Number of helpful votes.
 * @param total Number of total votes (helpful + not helpful). This number is assumed to be not lower than `positive`.
 */
case class Helpful(positive: Int, total: Int)

/** Companion object providing an implicit decoding and encoding to and from a textual representation of [[Helpful]]s.
 */
object Helpful {
  implicit val helpfulFormat : Encoder[Helpful] with Decoder[Helpful] =
    new Encoder[Helpful] with Decoder[Helpful] {
      override def apply(h: Helpful): Json = Encoder.encodeList[Int].apply(List(h.positive, h.total))
      override def apply(c: HCursor): Result[Helpful] = Decoder.decodeList[Int].map(
        l => Helpful(l.head, l.tail.head)
      ).apply(c)
    }
}
