package com.gitlab.edufuga.scala

import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, HCursor, Json}
import io.circe.generic.semiauto.{deriveDecoder, deriveEncoder}

import java.time.{Instant, LocalDateTime, ZoneId}

/** Review of an Amazon product.
 *
 * @param asin Amazon product identifier.
 * @param helpful Helpfulness rating of the review.
 * @param overall Rating of the Amazon product.
 * @param reviewText Text of the review.
 * @param reviewerID Identifier of the reviewer.
 * @param reviewerName Name of the reviewer (possibly not specified)
 * @param summary A summary of the review.
 * @param unixReviewTime Unix timestamp of the review (in seconds). This timestamp is assumed to be in UTC.
 */
case class Review(asin: String,
                  helpful: Helpful,
                  overall: Double,
                  reviewText: String,
                  reviewerID: String,
                  reviewerName: Option[String],
                  summary: String,
                  unixReviewTime: LocalDateTime)

/** Companion object providing an implicit decoding and encoding to and from a textual representation of [[Review]]s.
 */
object Review {
  implicit val reviewEncoder: Encoder[Review] = deriveEncoder[Review]
  implicit val reviewDecoder: Decoder[Review] = deriveDecoder[Review]

  implicit val dateFormat : Encoder[LocalDateTime] with Decoder[LocalDateTime] =
    new Encoder[LocalDateTime] with Decoder[LocalDateTime] {
      override def apply(d: LocalDateTime): Json = Encoder.encodeLong.apply(d.atZone(ZoneId.systemDefault()).toEpochSecond)
      override def apply(c: HCursor): Result[LocalDateTime] = Decoder.decodeLong.map(
        s => LocalDateTime.ofInstant(Instant.ofEpochSecond(s), ZoneId.systemDefault())
      ).apply(c)
    }
}
