package com.gitlab.edufuga.scala

import java.time.LocalDateTime
import scala.language.implicitConversions

/** Review entity of an Amazon product.
 *
 * This type corresponds to [[Review]] and is used merely to represent its persistence (e.g. to a database).
 *
 * @param id ID of the persisted entity. Empty if not yet persisted. This is maintained by the persistence mechanism.
 * @param asin Amazon product identifier.
 * @param helpfulPositive Positive rating of the review.
 * @param helpfulTotal Total rating of the review.
 * @param overall Rating of the Amazon product.
 * @param reviewText Text of the review.
 * @param reviewerID Identifier of the reviewer.
 * @param reviewerName Name of the reviewer (possibly not specified).
 * @param summary A summary of the review.
 * @param unixReviewTime Unix timestamp of the review (in seconds). This timestamp is assumed to be in UTC.
 */
case class ReviewEntity(id: Option[Int],              /** Different than in [[Review]]. */
                        asin: String,
                        helpfulPositive: Int,         /** Different than in [[Review]]. */
                        helpfulTotal: Int,            /** Different than in [[Review]]. */
                        overall: Double,
                        reviewText: String,
                        reviewerID: String,
                        reviewerName: Option[String],
                        summary: String,
                        unixReviewTime: LocalDateTime)

/** Companion object providing implicit conversions between [[Review]] and [[ReviewEntity]]. */
object ReviewEntity {
  // https://stackoverflow.com/questions/22367092/using-tupled-method-when-companion-object-is-in-class
  def tupled: ((Option[Int], String, Int, Int, Double, String, String, Option[String], String, LocalDateTime))
    => ReviewEntity = (ReviewEntity.apply _).tupled

  implicit def reviewToReviewEntity(r: Review): ReviewEntity =
    ReviewEntity(None, r.asin, r.helpful.positive, r.helpful.total, r.overall,
      r.reviewText, r.reviewerID, r.reviewerName, r.summary, r.unixReviewTime)

  implicit def reviewEntityToReview(r: ReviewEntity): Review =
    Review(r.asin, Helpful(r.helpfulPositive, r.helpfulTotal), r.overall,
      r.reviewText, r.reviewerID, r.reviewerName, r.summary, r.unixReviewTime)
}
