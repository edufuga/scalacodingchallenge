package com.gitlab.edufuga.scala

import slick.dbio.DBIO
import slick.jdbc.HsqldbProfile.api._
import slick.lifted.TableQuery

import java.time.{LocalDate, LocalDateTime, LocalTime, ZoneOffset}
import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.language.implicitConversions

/**
 * Finds the best Amazon reviews matching the specified criteria.
 *
 * @param dbProvider Provides the database with Amazon reviews.
 */
sealed case class BestMatchFinder(dbProvider: () => Database = () => Database.forConfig("hsqldb")) {
  /** Finds the best Amazon reviews matching the specified criteria.
   *
   * @param from Start date of the Amazon reviews to consider (inclusive). The start of the day is considered (00:00).
   * @param to End date of the Amazon reviews to consider (inclusive). The end of the day is considered (23:59).
   * @param minReviews Minimum number of reviews to consider. Products with a lower rating are thus left out.
   * @param limit Maximum number of results to consider.
   * @return List of best matches.
   */
  def findBestMatches(from: LocalDate, to: LocalDate, minReviews: Int, limit: Int) : Seq[BestMatch] = findBestMatches(
    LocalDateTime.of(from, LocalTime.MIN).atZone(ZoneOffset.UTC).toLocalDateTime,
    LocalDateTime.of(to, LocalTime.MAX).atZone(ZoneOffset.UTC).toLocalDateTime,
    minReviews,
    limit)

  private def findBestMatches(from: LocalDateTime, to: LocalDateTime, minReviews: Int, limit: Int) : Seq[BestMatch] = {
    val database = dbProvider()

    val reviews = TableQuery[ReviewTable]
    val filteredReviewsAction : DBIO[Seq[(String, Int, Double)]] = {
      reviews.filter(_.unixReviewTime >= from).filter(_.unixReviewTime <= to)
        .groupBy(_.asin)
        .map {
          case (id, elements) => (id, elements.length, elements.map(_.overall).avg.getOrElse(0.0))
        }
        .filter(_._2 >= minReviews)
        .sortBy(_._3.desc)
        .take(limit)
        .result
    }
    val filterReviewsFuture = database.run(filteredReviewsAction)
    val bestMatchesRaw = Await.result(filterReviewsFuture, Duration.Inf)
    val bestMatches = bestMatchesRaw.map(r => BestMatch(r._1, r._3))
    database.close()
    bestMatches
  }
}
