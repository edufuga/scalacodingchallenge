package com.gitlab.edufuga.scala

import cats.effect.IO
import cats.effect.IO.ioEffect
import com.twitter.finagle.Service
import com.twitter.finagle.http._
import io.finch._
import io.finch.circe.dropNullValues._

import java.time.LocalDate

/** Webservice for returning the best rated products based on Amazon reviews. */
sealed case class BestMatchService(bestMatchFinder: (LocalDate, LocalDate, Int, Int) => Seq[BestMatch])
  extends Endpoint.Module[IO] {
  /** Request handler for providing the best rated Amazon products.
   *
   * The accepted request [[BestMatchRequest]] will be answered with the corresponding response [[BestMatch]].
   *
   * @return the best rated Amazon products with their average rating
   */
  def bestMatchRequestHandler: Endpoint[IO, List[BestMatch]] =
    post("amazon" :: "best-rated" :: jsonBody[BestMatchRequest]) {
      request: BestMatchRequest =>
        val response: List[BestMatch] =
          bestMatchFinder(request.start, request.end, request.min_number_reviews, request.limit)
            .toList
        Ok(response)
    } handle {
      case e: Exception =>
        e.printStackTrace()
        BadRequest(e)
    }

  /** Webservice for returning the best rated products based on Amazon reviews as JSON output. */
  def service: Service[Request, Response] = bestMatchRequestHandler.toServiceAs[Application.Json]
}
