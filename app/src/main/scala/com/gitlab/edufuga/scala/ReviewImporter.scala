package com.gitlab.edufuga.scala

import io.circe.parser.decode
import slick.jdbc.HsqldbProfile.api._

import java.nio.file.{Files, Path}
import java.time.Instant
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Future}
import scala.io.Source
import scala.util.{Failure, Success}

/** Imports Amazon reviews from a file to a database.
 *
 * @param dbProvider Provides the database where the Amazon reviews will be imported.
 */
sealed case class ReviewImporter(dbProvider: () => Database = () => Database.forConfig("hsqldb")) {
  setupDatabase()

  private def setupDatabase(): Unit = {
    dropDatabase()
    createDatabase()
  }

  private def dropDatabase(): Unit = {
    val db: Database = dbProvider()
    val reviews = TableQuery[ReviewTable]
    val dropAction: DBIO[Unit] = DBIO.seq(reviews.schema.dropIfExists)
    val dropFuture: Future[Unit] = db.run(dropAction)
    Await.result(dropFuture, Duration.Inf)
    db.close()
  }

  private def createDatabase(): Unit = {
    val db: Database = dbProvider()
    val reviews = TableQuery[ReviewTable]
    val setupAction: DBIO[Unit] = DBIO.seq(reviews.schema.createIfNotExists)
    val setupFuture: Future[Unit] = db.run(setupAction)
    Await.result(setupFuture, Duration.Inf)
    db.close()
  }

  /** Imports the Amazon reviews in the given file.
   *
   * This assumes that every line in the file contains a valid JSON object compatible with [[Review]].
   *
   * @param filePath Path to the file to be imported.
   * @return Number of imported Amazon reviews.
   */
  def importReviewsFrom(filePath: Path): Int = {
    if (Files.notExists(filePath)) {
      throw new RuntimeException("The file path you gave me doesn't exist.")
    }

    val source = Source.fromFile(filePath.toFile)
    val total = importReviewsFrom(source.getLines())
    source.close()
    total
  }

  /** Imports the Amazon reviews in the given iterator.
   *
   * This assumes that every line in the file contains a valid JSON object compatible with [[Review]].
   *
   * @param lines The lines to be imported.
   * @return Number of imported Amazon reviews.
   */
  def importReviewsFrom(lines: Iterator[String], chunkSize: Int = 2*1024): Int =
    lines.grouped(chunkSize)
      .map {
        chunk: Seq[String] => importReviewsChunk(decodeReviews(chunk.iterator))
      }
      .collect { f: Future[Option[Int]] =>
        val result = Await.result(f, Duration.Inf)
        val number = result.getOrElse(0)
        println(Instant.now().toString + "\tImported " + number + " reviews.")
        number
      }
      .sum

  private def decodeReviews(lines: Iterator[String]): Iterator[Review] = {
    lines.filter(_.nonEmpty)
      .map(line => {
        decode[Review](line)
      })
      .collect {
        case Right(decodedJson) => decodedJson
      }
  }

  private def importReviewsChunk(reviewsChunk: Iterator[Review]): Future[Option[Int]] = {
    val db: Database = dbProvider()

    val reviews = TableQuery[ReviewTable]

    val insertReviewsAction = {
      reviews ++= reviewsChunk.map(ReviewEntity.reviewToReviewEntity).toList
    }

    val insertReviewsFuture: Future[Option[Int]] = db.run(insertReviewsAction)

    insertReviewsFuture onComplete {
      case Success(_) =>
        db.close()
      case Failure(exception) =>
        println("Something went wrong importing the review chunk: " + exception.getMessage)
        db.close()
    }

    insertReviewsFuture
  }
}
