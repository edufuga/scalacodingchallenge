package com.gitlab.edufuga.scala

import org.junit.runner.RunWith
import org.scalatest._
import org.scalatestplus.junit.JUnitRunner
import slick.jdbc.HsqldbProfile.api._

import scala.io.Source

@RunWith(classOf[JUnitRunner])
class ReviewImporterSpec extends featurespec.AnyFeatureSpec with GivenWhenThen {
  info("As a developer")
  info("I want to be able to import reviews of Amazon products")
  info("so I can use all previously entered reviews to enable an informed purchase decision")
  info("and be happy with this functionality")

  Feature("Importing reviews") {
    Scenario("Someone imports a single review") {
      Given("a single review")
      val review: String =
        """{"asin":"B000Q75VCO",
          |"helpful":[16,40],
          |"overall":2.0,
          |"reviewText":"Words are in my not-so-humble opinion, the most inexhaustible form of magic we have...",
          |"reviewerID":"B07844AAA04E4",
          |"reviewerName":"Albus Dumbledore",
          |"summary":"You must read Harry Potter",
          |"unixReviewTime":1475261866}""".stripMargin

      When("the review is imported to the database")
      val reviewImporter: ReviewImporter = ReviewImporter(() => Database.forConfig("hsqldb_test"))
      val num = reviewImporter.importReviewsFrom(Iterator(review))

      Then("the number of imported reviews is the expected one")
      assert(num == 1)
    }

    Scenario("Someone imports several reviews") {
      Given("several reviews")
      val reviews: Iterator[String]  = Source.fromResource("video_game_reviews_example.json").getLines()

      When("the reviews are imported to the database")
      val reviewImporter: ReviewImporter = ReviewImporter(() => Database.forConfig("hsqldb_test"))
      val num = reviewImporter.importReviewsFrom(reviews)

      Then("the number of imported reviews is the expected one")
      assert(num == 15)
    }
  }
}
