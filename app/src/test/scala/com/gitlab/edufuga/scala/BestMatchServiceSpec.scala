package com.gitlab.edufuga.scala

import com.twitter.io.Buf
import io.finch.{Application, Input}
import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.matchers.should.Matchers._
import org.scalatestplus.junit.JUnitRunner
import java.time.LocalDate
import scala.language.implicitConversions

@RunWith(classOf[JUnitRunner])
class BestMatchServiceSpec extends featurespec.AnyFeatureSpec with GivenWhenThen {
  info("As a user")
  info("I want to be able to interact with the webservice")
  info("in order to obtain the best rated Amazon products")
  info("based on all the available Amazon reviews")
  info("and be happy with the result")

  Feature("Finding the best rated products via the webservice's endpoint") {
    Scenario("The user searches for the best rated products within a time range and other criteria") {
      Given("a webservice endpoint for the best matches")
      val bestMatchFinder: (LocalDate, LocalDate, Int, Int) => Seq[BestMatch] =
        (_, _, _, _) => List(BestMatch("BEST_PRODUCT", 5.0))
      val bestMatchService: BestMatchService = BestMatchService(bestMatchFinder)
      val bestMatchRequestHandler = bestMatchService.bestMatchRequestHandler

      When("the user searches for the best rated products")
      val result = bestMatchRequestHandler(Input.post("/amazon/best-rated")
        .withBody[Application.Json](
          Buf.Utf8("""{"start": "01.01.2010", "end": "31.12.2020", "limit": 2, "min_number_reviews": 2}"""))
        )
        .awaitValueUnsafe()
        .getOrElse(List.empty)

      Then("the expected best matches are found")
      result should contain (BestMatch("BEST_PRODUCT", 5.0))
    }
  }
}
