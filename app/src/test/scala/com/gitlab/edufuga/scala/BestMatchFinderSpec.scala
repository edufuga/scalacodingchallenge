package com.gitlab.edufuga.scala

import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.matchers.should.Matchers._
import org.scalatestplus.junit.JUnitRunner
import slick.jdbc.HsqldbProfile.api._

import java.time.LocalDate
import scala.io.Source
import scala.language.implicitConversions

@RunWith(classOf[JUnitRunner])
class BestMatchFinderSpec extends featurespec.AnyFeatureSpec with GivenWhenThen {
  info("As a user")
  info("I want to be able to use all previously entered reviews to enable an informed purchase decision")
  info("by selecting the best matching products according to my own criteria")
  info("and be happy with the Amazon shopping tour")

  Feature("Finding the best matching products") {
    Scenario("The user searches for the best matching products within a time range and other criteria") {
      Given("a database with previously imported reviews")
      val dbProvider = () => Database.forConfig("hsqldb_test")
      val reviewImporter: ReviewImporter = new ReviewImporter(dbProvider)
      val reviews: Iterator[String]  = Source.fromResource("video_game_reviews_example.json").getLines()
      reviewImporter.importReviewsFrom(reviews)

      When("the user searches for the best matches within 10+1 years")
      val bestMatchFinder: BestMatchFinder = new BestMatchFinder(dbProvider)
      val bestMatches = bestMatchFinder.findBestMatches(
        LocalDate.of(2010, 1, 1),
        LocalDate.of(2020, 12, 30),
        2,
        2
      )

      Then("the expected best matches are found")
      bestMatches should (
        contain only (
          new BestMatch("B000JQ0JNS", 4.5),
          new BestMatch("B000NI7RW8", 3.6666666666666665)
        ) and have size 2)
    }
  }
}
