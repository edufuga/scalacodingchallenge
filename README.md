# Introduction

This is my solution to the Scala Coding Challenge.

# What can the program do?

The program imports all Amazon reviews in a given plain text file with JSON-formatted lines into a database and runs a web service with an entrypoint that returns the best products matching certain criteria.

# How do I build the program?

> Note: The following commands assume that the working directory is the root folder of the project.

First of all, the console application has to be built from the [source code](https://gitlab.com/edufuga/scalacodingchallenge.git). The shell script `build.sh` does exactly that:

    ./build.sh

This should look like this: [![asciicast](https://asciinema.org/a/mcEsBus4hvtGDjxJHNny8UvHP.svg)](https://asciinema.org/a/mcEsBus4hvtGDjxJHNny8UvHP).

# How do I start the program?

Once the program has been built, run the following command to import the reviews and start the webservice:

    ./app/build/distributions/app/bin/app ./app/src/main/resources/video_game_reviews_example.json

This should look like this: [![asciicast](https://asciinema.org/a/6w5UZ6y1VnW2CpoB62asaqdNS.svg)](https://asciinema.org/a/6w5UZ6y1VnW2CpoB62asaqdNS).

# How do I use the program?

Once the program is running, the webservice accepts HTTP POST requests like this one:

```http
POST /amazon/best-rated HTTP/1.1
Host: localhost:8080
Content-Type: application/json

{
  "start": "01.01.2010",
  "end": "31.12.2020",
  "limit": 2,
  "min_number_reviews": 2
}
```

> Note: The script `example.sh` does exactly that using `cURL`.


The result should be equal to the following:

    [{"asin":"B0000DEVGP","averageRating":3.0514389679126697},{"asin":"B000BHP43U","averageRating":3.0510428100987927}]

> Note: Currently the output of my program shows the result in a flattened list, i.e. a single line. The pretty-printing of the JSON output mentioned [in the documentation](https://finagle.github.io/finch/user-guide.html#encoding-to-json) didn't quite work out yet. I assume that's because I don't have an encoder and decoder pair for the `List[Review]`s, but I haven't followed this issue further due to time reasons.

# Where is the data stored?

The file with JSON-formatted reviews is imported into a database. Specifically, this is a disk-based HyperSQL database. The files it generates are stored directly within the *home directory* of the user starting the program. This generates the following files:

```
drwxr-xr-x   2 eduard  staff    64B Jan  2 19:28 amazon.tmp
-rw-r--r--   1 eduard  staff   731M Jan  2 19:35 amazon.script
-rw-r--r--   1 eduard  staff   106B Jan  2 19:35 amazon.properties
-rw-r--r--   1 eduard  staff   7.8M Jan  2 19:36 amazon.log
```

> Please **delete** these files after stopping the program (`Control+C`): `rm -rf ~/amazon.*`. The code should actually delete the previously created database table, but I've observed that with HyperSQL it doesn't really work as expected (it *did* work well with an H2 database, but that had other more serious shortcomings).

# Rationale and Review

I've written the program using frameworks and tools I didn't know anything about ([Slick](https://scala-slick.org/), [Circe](https://circe.github.io/circe/), [Finch](https://finagle.github.io/finch/)). I've chosen these tools because, as far as I can tell, they are mature Scala-technologies that fit well within the functional paradigm. At the same time, they were comparatively easy to get into (on a superficial level), so that I could start using them without a too steep learning curve in the context and time limits of a coding challenge.

That said, I'm aware that the code isn't as neat (clean), well-organized and decoupled as is could or even should be. One example is the comparatively low amount of tests. Due to me being initially unfamiliar with the aforementioned tools, I decided against a top-buttom approach; instead, everything has been written in a buttom-up way. Additionally, the absence of further tests and more test coverage is also due to time constraints. Nonetheless, the most essential functionality *is* tested at a global level.
