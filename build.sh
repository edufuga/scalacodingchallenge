echo "Let's clean up"
./gradlew clean
echo ""

echo "Let's build something nice"
./gradlew build
echo ""

cd app/build/distributions
unzip app.zip

echo "Bye"
